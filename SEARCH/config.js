//config.js

"use strict";

var config = {

	ENVIRONMENT 					: process.env.NODE_ENV || 'development',

	/*
		Environment specific settings
		These will be loaded after common , and will overwrite common settings 

		PUT settings according to the env block
	*/

	'development' : {
		ELASTICSEARCH :{
			BULK_SIZE: 2,
			BULK_SIZE_MB: 10,
			NUM_SHARDS: 10,
			BULK_CONCURRENCY: 2,
			REQUEST_TIMEOUT: 60000	//milliseconds
		},

		RABBITMQ : {
        	CONNECT_STRING          : 'amqp://guest:guest@localhost:5672?heartbeat=60',
        	RETRY_INTERVAL          : 5000,     // milliseconds
    		PREFETCH_COUNT			: 100,
    		QUEUE_NAME				: 'dump10',
    		EXCHANGE_NAME			: '',
    		EXCHANGE_KEY			: ''
    	},

    	FLAGS:{
    		BULK_DECISION: 'LENGTH',//'LENGTH' or 'MEMORY' supported, length uses ELASTICSEARCH.BULK_SIZE, memory used BULK_SIZE_MB
    		MULTI_INDEX_SUPPORT : true //set this flag to true or false. if true it will pick only the config. If set to false, it will fetch settings from DB
    	},

		INFRA: [{
			host: 'localhost:9200',
			log: false,
			requestTimeout: 120000, //120ms
			index: 'catalog',
			type: 'refiner',
			client: ''
		}, {
			host: 'localhost:9200',
			log: false,
			requestTimeout: 120000, //120ms
			index: 'catalog222',
			type: 'refiner',
			client: ''
		}]
	},

	'production':{
		ELASTICSEARCH :{
			URL: 'localhost:9200',
			INDEX: 'catalog',
			TYPE: 'refiner',
			BULK_SIZE: 2,
			BULK_SIZE_MB: 10,
			NUM_SHARDS: 10,
			BULK_CONCURRENCY: 8,
			REQUEST_TIMEOUT: 60000	//milliseconds
		},

		RABBITMQ : {
        	CONNECT_STRING          : 'amqp://guest:guest@localhost:5672?heartbeat=60',
        	RETRY_INTERVAL          : 5000,     // milliseconds
    		PREFETCH_COUNT			: 100,
    		QUEUE_NAME				: 'dump10',
    		EXCHANGE_NAME			: '',
    		EXCHANGE_KEY			: ''
    	},

    	FLAGS:{
    		BULK_DECISION: 'LENGTH',//'LENGTH' or 'MEMORY' supported, length uses ELASTICSEARCH.BULK_SIZE, memory used BULK_SIZE_MB
    		MULTI_INDEX_SUPPORT : true //set this flag to true or false. if true it will pick only the config. If set to false, it will fetch settings from DB
    	},

		INFRA: [{
			host: 'localhost:9200',
			log: false,
			requestTimeout: 120000, //120ms
			index: 'catalog',
			type: 'refiner',
			client: ''
		}, {
			host: 'localhost:9200',
			log: false,
			requestTimeout: 120000, //120ms
			index: 'catalog222',
			type: 'refiner',
			client: ''
		}]
	}
};

//module.exports = config;


var load = function(){
	return config[config.ENVIRONMENT];
};

module.exports = load();


